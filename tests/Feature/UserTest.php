<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_cant_see_users()
    {

        $user = User::factory()->create([
            'type' => 2
        ]);

        $response = $this->actingAs($user)->get('/dashboard');

        $response->assertStatus(302);
    }


    public function test_user_cant_update_users()
    {

        $user = User::factory()->create([
            'type' => 2
        ]);

        $user->name = 'Rachid';

        $response = $this->actingAs($user)->put('/users/'.$user->id , $user->toArray() );

        $response->assertStatus(302);
    }
    
    public function test_user_cant_delete_temporary_users()
    {

        $user = User::factory()->create([
            'type' => 2
        ]);

        $response = $this->actingAs($user)->delete('/users/'.$user->id);

        $response->assertStatus(302);
    }


    public function test_user_cant_delete_permanently_users()
    {

        $user = User::factory()->create([
            'type' => 2
        ]);

        $response = $this->actingAs($user)->delete('/users/'.$user->id . '/delete');

        $response->assertStatus(302);

    }

    public function test_user_cant_restore_users()
    {

        $user = User::factory()->create([
            'type' => 2
        ]);

        $response = $this->actingAs($user)->post('/users/'.$user->id . '/restore');

        $response->assertStatus(302);

    }
}
