<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class AdminTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_admin_can_see_users()
    {

        $user = User::factory()->create([
            'type' => 1
        ]);

        $response = $this->actingAs($user)->get('/dashboard');

        $response->assertSee($user->name);
    }

    public function test_admin_can_update_users()
    {

        $user = User::factory()->create([
            'type' => 1
        ]);

        $user->name = 'Rachid';

        $this->actingAs($user)->put('/users/'.$user->id , $user->toArray() );

        $this->assertDatabaseHas('users',['id'=> $user->id , 'name' => 'Rachid']);
    }
    public function test_admin_can_delete_temporary_users()
    {

        $user = User::factory()->create([
            'type' => 1
        ]);

        $this->actingAs($user)->delete('/users/'.$user->id);

        $this->assertSoftDeleted('users',$user->toArray());
    }

    public function test_admin_can_delete_permanently_users()
    {

        $user = User::factory()->create([
            'type' => 1
        ]);

        $this->actingAs($user)->delete('/users/'.$user->id . '/delete');

        $this->assertDatabaseMissing('users',['id'=> $user->id]);

    }

    public function test_admin_can_restore_users()
    {

        $user = User::factory()->create([
            'type' => 1
        ]);

        $this->actingAs($user)->post('/users/'.$user->id . '/restore');

        $this->assertDatabaseHas('users',['deleted_at'=> null]);

    }
}
