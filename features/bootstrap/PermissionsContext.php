<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Tester\Exception\PendingException;

use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

use Behat\MinkExtension\Context\MinkContext;
use PHPUnit\Framework\Assert as Assert;



/**
 * Defines application features from the specific context.
 */
class PermissionsContext extends MinkContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You  also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */


    public function __construct()
    {
    }

    /**
     * @Given is admin authenticated
     */
    public function isAdminAuthenticated()
    {
        $this->visit('/login');
        $this->fillField('email', 'ayoub@example.com');
        $this->fillField('password', 'ayoub123');
        $this->pressButton('Log in');
    }

    /**
     * @Given admin have access to dashboard
     */
    public function adminHaveAccessToDashboard()
    {
        $this->visit('/dashboard');
    }

    /**
     * @Then admin can update
     */
    public function adminCanUpdate()
    {
        $this->visit('/dashboard');
        $this->clickLink('update');
        $this->fillField('email', 'rachid@example.com');
        $this->pressButton('update');
    }

    /**
     * @Then admin can delete
     */
    public function adminCanDelete()
    {
        $this->clickLink('delete');
        $this->pressButton('delete');
    }

    /**
     * @Then admin can restore
     */
    public function adminCanRestore()
    {
        $this->visit('/users');
        $this->pressButton('restore');
    }

    /**
     * @Given is user authenticated
     */
    public function isUserAuthenticated()
    {
        $this->visit('/login');
        $this->fillField('email', 'morad@example.com');
        $this->fillField('password', 'morad123');
        $this->pressButton('Log in');
    }

    /**
     * @Given user cant have access to dashboard
     */
    public function userCantHaveAccessToDashboard()
    {
        $this->visit('/dashboard');
    }

    private function takeScreenshot()
    {
        $baseUrl = $this->getMinkParameter('base_url');
        $fileName = date('d-m-y') . '-' . uniqid() . '.png';
        $filePath = 'public/';
        $this->saveScreenshot($fileName, $filePath);
        print 'Screenshot at: ' . $baseUrl . '/' . $fileName;
    }
}
