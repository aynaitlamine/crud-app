@javascript
Feature: Testing permissions 
    Scenario: testing admin permissions 
    Given is admin authenticated  
    And admin have access to dashboard
    Then admin can update 
    And admin can delete 
    And admin can restore 

    Scenario: testing user permissions 
    Given is user authenticated  
    And user cant have access to dashboard
