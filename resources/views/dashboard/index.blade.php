<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(session()->has('message'))
            <div class="my-4 alert alert-success">
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" class="stroke-current flex-shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                    <span> {{ session()->get('message') }}</span>
                </div>
            </div>
            @endif
            <div class="my-4 flex justify-end btn-group">
                <a href="{{ route('dashboard') }}" class="btn btn-xs {{ empty(request()->type) ? 'btn-active' : 'btn-ghost' }}">All</a>
                <a href="{{ route('dashboard', ['type' => 1]) }}" class="btn btn-xs {{ request()->type == 1 ? 'btn-active' : 'btn-ghost' }}">Admins</a>
                <a href="{{ route('dashboard', ['type' => 2]) }}" class="btn btn-xs {{ request()->type == 2 ? 'btn-active' : 'btn-ghost' }}">Users</a>
            </div>
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="overflow-x-auto w-full">
                    <table class="table w-full">
                        <!-- head -->
                        <thead>
                            <tr>
                                <th>Avatar</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- row 1 -->
                            @if(!empty($users))
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                        <div class="flex items-center space-x-3">
                                            <div class="avatar">
                                                <div class="w-12 h-12 mask mask-squircle">
                                                    <img src="{{ $user->avatar}}" alt="Avatar Tailwind CSS Component">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $user->name}}

                                    </td>
                                    <td>{{ $user->type == 1 ? 'Admin' : 'User'}}</td>
                                    <th>
                                        <!-- The button to open modal -->
                                        <a href="#details-modal-{{ $user->id}}" class="btn btn-ghost btn-xs">details</a>
                                        <a href="#update-modal-{{ $user->id}}" class="btn btn-success btn-xs">update</a>
                                        <a href="#delete-modal-{{ $user->id}}" class="btn btn-error btn-xs">delete</a>
                                    </th>
                                </tr>

                                <!-- Put this part before </body> tag -->
                                <div class="modal" id="details-modal-{{ $user->id}}">
                                    <div class="modal-box items-center text-center">

                                        <div class="avatar">
                                            <div class="w-24 rounded-full ring ring-primary ring-offset-base-100 ring-offset-2">
                                                <img src="{{ $user->avatar }}">
                                            </div>
                                        </div>
                                        <h3 class="font-bold text-lg">{{ $user->name }}</h3>
                                        <p class="py-2">{{ $user->email }}</p>
                                        <div class="badge badge-ghost">{{ $user->type == 1 ? 'admin' : 'user' }}</div>
                                        <div class="flex justify-center modal-action">
                                            <a href="#" class="btn btn-error btn-xs">close!</a>
                                        </div>
                                    </div>
                                </div>

                                <!-- Put this part before </body> tag -->
                                <div class="modal flex flex-col justify-center space-x-3" id="update-modal-{{ $user->id}}">
                                    <div class="modal-box">
                                        <form action="{{ route('users.update', $user) }}" method="POST" id="updateForm-{{ $user->id}}" enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf
                                            <div class="form-control">
                                                <label class="label">
                                                    <span class="label-text">Name</span>
                                                </label>
                                                <input type="text" name="name" placeholder="{{ $user->name }}" value="{{ $user->name }}" class="input input-bordered w-full max-w-xs">
                                            </div>

                                            <div class="form-control">
                                                <label class="label">
                                                    <span class="label-text">Email</span>
                                                </label>
                                                <input type="text" name="email" placeholder="{{ $user->email }}" value="{{ $user->email }}" class="input input-bordered w-full max-w-xs">
                                            </div>

                                            <div class="form-control">
                                                <label class="label">
                                                    <span class="label-text">Password</span>
                                                </label>
                                                <input type="password" name="password" placeholder="Password" class="input input-bordered w-full max-w-xs">
                                            </div>


                                            <div class="form-control">
                                                <label class="label">
                                                    <span class="label-text">Type</span>
                                                </label>
                                                <select name="type" class="select w-full max-w-xs select-warning">
                                                    <option value="{{ $user->type == 1 ? 1 : 2 }}" selected>{{ $user->type == 1 ? 'Admin' : 'User' }}</option>
                                                    <option value="{{ $user->type == 2 ? 1 : 2 }}">{{ $user->type == 1 ? 'User' : 'Admin' }}</option>
                                                </select>
                                            </div>
                                            <div class="form-control">
                                                <label class="label">
                                                    <span class="label-text">Choose an avatar</span>
                                                </label>
                                                <input type="file" name="avatar" class="block w-full text-sm text-slate-500
        file:mr-4 file:py-2 file:px-4
        file:rounded-full file:border-0
        file:text-sm file:font-semibold
        file:bg-violet-50 file:text-violet-700
        hover:file:bg-violet-100
        " />
                                            </div>
                                        </form>

                                        <div class="flex justify-center modal-action">
                                            <button type="submit" form="updateForm-{{ $user->id}}" class="btn btn-success btn-xs">update!</button>
                                            <a href="#" class="btn btn-error btn-xs">close!</a>
                                        </div>
                                    </div>
                                </div>

                                <!-- Put this part before </body> tag -->
                                <div class="modal" id="delete-modal-{{ $user->id}}">
                                    <div class="modal-box items-center text-center">
                                        <h2 class="font-bold text-lg">Delete!</h2>
                                        <p>Are your sure you want to delete this user.</p>
                                        <div class="flex justify-center modal-action">
                                            <form action="{{ route('users.destroy', $user) }}" method="post">
                                                @method("DELETE")
                                                @csrf
                                                <button type="submit" class="btn btn-error btn-xs">delete</button>
                                            </form>

                                            <a href="#" class="btn btn-success btn-xs">close!</a>
                                        </div>

                                    </div>

                                </div>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-4 lg:px-4">
            @if(!empty($users))
                {{ $users->withQueryString()->links()}}
            @endif
        </div>
    </div>

</x-app-layout>