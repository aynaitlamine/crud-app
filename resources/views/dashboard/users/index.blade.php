<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Deleted users') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(session()->has('message'))
                <div class="my-4 alert alert-success">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" class="stroke-current flex-shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        <span> {{ session()->get('message') }}</span>
                    </div>
                </div>
            @endif
            <div class="my-4 flex justify-end btn-group">
                <a href="{{ route('users.index') }}" class="btn btn-xs {{ empty(request()->type) ? 'btn-active' : 'btn-ghost' }}">All</a>
                <a href="{{ route('users.index', ['type' => 1]) }}" class="btn btn-xs {{ request()->type == 1 ? 'btn-active' : 'btn-ghost' }}">Admins</a>
                <a href="{{ route('users.index', ['type' => 2]) }}" class="btn btn-xs {{ request()->type == 2 ? 'btn-active' : 'btn-ghost' }}">Users</a>
            </div>
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="overflow-x-auto w-full">
                    <table class="table w-full">
                        <!-- head -->
                        <thead>
                            <tr>
                                <th>Avatar</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- row 1 -->
                            @if(!empty($users))
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                        <div class="flex items-center space-x-3">
                                            <div class="avatar">
                                                <div class="w-12 h-12 mask mask-squircle">
                                                    <img src="{{ $user->avatar}}" alt="Avatar Tailwind CSS Component">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $user->name}}

                                    </td>
                                    <td>{{ $user->type == 1 ? 'Admin' : 'User'}}</td>
                                    <th>
                                        <!-- The button to open modal -->
                                        <form action="{{ route('users.restore', $user) }}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-success btn-xs">restore</button>
                                        </form>
                                        <form action="{{ route('users.delete', $user) }}" method="POST">
                                            @method("DELETE")
                                            @csrf
                                            <button type="submit" class="btn btn-error btn-xs">delete permanently</button>
                                        </form>
                                    </th>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-4 lg:px-4">
        @if(!empty($users))
            {{ $users->links()}}
        @endif
        </div>
    </div>

</x-app-layout>