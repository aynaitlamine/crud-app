<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserUpdateRequest;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (empty(request()->type)) {

            $users = User::onlyTrashed()->paginate(5);
            return view('dashboard.users.index', [
                'users' => $users
            ]);
        } else {
            $users = User::onlyTrashed()->where('type', request()->type)->paginate(5);
            return view('dashboard.users.index', [
                'users' => $users
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('dashboard.users.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('dashboard.users.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $data = User::findOrFail($user->id);

        if (empty($request->password)) {
            $request->merge([
                'password' => $data->password
            ]);
        }


        $data->update($request->all());




        return  back()->with('message', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {

        User::findOrFail($user)->delete();

        return back()->with('message', 'User deleted successfully');
    }

    public function restore($user)
    {
        User::withTrashed()->findOrFail($user)->restore();
        return back()->with('message', 'User restored successfully');;
    }

    public function delete($user)
    {
        User::withTrashed()->findOrFail($user)->forceDelete();
        return back()->with('message', 'User deleted permanently successfully');;
    }
}
