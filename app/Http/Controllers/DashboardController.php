<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (empty(request()->type)){
            $users = User::paginate(5);
            return view('dashboard.index', [
                'users' => $users
            ]);
        }else{
            $users = User::where('type', request()->type)->paginate(5);
            return view('dashboard.index', [
                'users' => $users
            ]);
        }
    }


}
